output "k3s_server_ip" {
    value = aws_instance.k3s.public_ip
}