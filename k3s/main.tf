provider "aws" {
    region = "us-east-1"
}

resource "aws_vpc" "network" {
    cidr_block = "10.0.0.0/16"
}

resource "aws_subnet" "subnet" {
    vpc_id = aws_vpc.network.id
    cidr_block = "10.0.1.0/24"
    tags = {
        Name = "a Subnet"
    }
    depends_on = [aws_vpc.network]
}

resource "aws_internet_gateway" "gw" {
    vpc_id = aws_vpc.network.id
}

resource "aws_route" "to_gw" {
    route_table_id = aws_vpc.network.default_route_table_id
    destination_cidr_block = "0.0.0.0/0"
    gateway_id = aws_internet_gateway.gw.id
    depends_on = [aws_internet_gateway.gw, aws_vpc.network]
}

data "aws_ami" "ubuntu" {
  most_recent = true

  filter {
    name   = "name"
    values = ["ubuntu/images/hvm-ssd/ubuntu-focal-20.04-amd64-server-*"]
  }

  filter {
    name   = "virtualization-type"
    values = ["hvm"]
  }

  owners = ["099720109477"]
}

resource "aws_security_group" "allow_communication" {
    name = "allow_communication"
    description = "allow k3s to communicate with internet"
    vpc_id = aws_vpc.network.id
    ingress {
        description = "K3S API communication"
        from_port = 6443
        to_port = 6443
        protocol = "tcp"
        cidr_blocks = ["0.0.0.0/0"]
    }
    ingress {
        description = "K3S HTTP communication"
        from_port = 80
        to_port = 80
        protocol = "tcp"
        cidr_blocks = ["0.0.0.0/0"]
    }
    ingress {
        description = "allow ssh"
        from_port = 22
        to_port = 22
        protocol = "tcp"
        cidr_blocks = ["0.0.0.0/0"]
    }
    egress {
        from_port   = 0
        to_port     = 0
        protocol    = "-1"
        cidr_blocks = ["0.0.0.0/0"]
    }
    tags = {
        Name = "Allow k3s communcation"
    }
    depends_on = [aws_vpc.network]
}

resource "aws_key_pair" "deploy_key" {
    key_name = "deploy_key"
    public_key = var.ssh_key
}

resource "aws_instance" "k3s" {
    ami = data.aws_ami.ubuntu.id
    instance_type = "t3.medium"
    tags = {
        Name = "k3s Server"
    }
    vpc_security_group_ids = [aws_security_group.allow_communication.id]
    subnet_id = aws_subnet.subnet.id
    root_block_device { 
        volume_size = 64
        volume_type = "gp2"
    }
    associate_public_ip_address = true
    key_name = "deploy_key"
    depends_on = [aws_key_pair.deploy_key, aws_security_group.allow_communication, aws_subnet.subnet]
    user_data = file("user-data.sh")
    provisioner "local-exec" {
        command = "sleep 360 && ssh -i ${var.ssh_private_key_location} ubuntu@${aws_instance.k3s.public_ip} 'sudo chmod 777 /etc/rancher/k3s/k3s.yml' && scp -i ${var.ssh_private_key_location} ubuntu@${aws_instance.k3s.public_ip}:/etc/rancher/k3s/k3s.yaml kubeconfig"
    }
}