variable "cluster_name" {
    description = "the name of the infra you want to provision"
    default = "k3s"
}
variable "ssh_key" {
    description = "the ssh key to provision"
    default = "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABgQDGNVEUBvGGnOg2qY+Mo5fyTloqRjOb9suwbqU6ga4Qyh+aCq1RIbPk/eEVsDFxlc5PM+TDM0016Qp2cTxHci6WMJTN5arQiTIFrdoDAEmPwPrH1/5K8lG+qT94ZQs0O0h7vNdV1rm6oHC8rs68/lNNGQzzzGxsAfszWIpYNcq79c2cCXV7bYEcKDU8vYkTaMDiFU1jILxep0L4i2KlOokqXyQAP5coEdfRe1/ygfTdHRgO3DeHnWfIMI2aguM0rT+i/Qqd+L+jVe8cmuYkLEfjPoq1cOTZUaDT/UlDsE/UxiaxCuGR9mfeAm+P88bzdlzbZ8HEPfNAk3u8llyPijoa0rx4yeTKLIiniPZwN2GLpTCOhV7B/Y/KMXA2vB8SO3NdQYqu9nEHxtLSwFEwp1O/MM+67WqXD5znStbdTWGjeTCCFYcHR9IVvNvhBzzEbfNqFUfc5GzmEGGG8OHIMDEys3iohicOY0DxTyby6O8xawnsnQe3t26KZFRHKQ7HVCU="   
}

variable "ssh_private_key_location" {
    description = "the location of the ssh private key"
    default = "~/.ssh/deploy_keky"
}