variable "website_url" {
    description = "The website URL for this deployment"
}

variable "region" {
    description = "The region you want to deploy the s3 origin at"
    default = "us-east-1"
}