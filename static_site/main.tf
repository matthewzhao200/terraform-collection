provider "aws" {
    region = var.region
}

resource "aws_s3_bucket" "website" {
    bucket = var.website_url
    acl = "private"   
}

resource "aws_cloudfront_distribution" "website" {
    origin = aws_s3_bucket.website.bucket_regional_domain_name
    origin_id
}

data "aws_iam_policy_document" "s3_policy" {
    statement {
        actions = ["s3.GetObject"]
        resources = ["${aws_s3_bucket.website.arn}/*"]
    }
    principals {
        type = "AWS"
        identifiers = []
    }
}